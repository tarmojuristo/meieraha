window.data = {}

window.data.expenses = {
  "children": [
    {
      "children": [
        {
          "disabled": [
            {
              "label_et": "Laste ja perede programm",
              "in": 0,
              "amount": 774213000
            }
          ],
          "label_et": "PEREPOLIITIKA",
          "in": 0,
          "amount": 774213000
        },
        {
          "disabled": [
            {
              "label_et": "TÃ¶Ã¶turuprogramm",
              "in": 0,
              "amount": 675085000
            }
          ],
          "label_et": "TÃ–Ã–TURG",
          "in": 0,
          "amount": 675085000
        },
        {
          "disabled": [
            {
              "label_et": "Sotsiaalkindlustuse programm",
              "in": 0,
              "amount": 2648232000
            },
            {
              "label_et": "Hoolekande programm",
              "in": 0,
              "amount": 88648000
            },
            {
              "label_et": "Soolise vÃµrdÃµiguslikkuse programm",
              "in": 0,
              "amount": 1849000
            }
          ],
          "label_et": "SOTSIAALNE KAITSE",
          "in": 0,
          "amount": 2738729000
        },
        {
          "disabled": [
            {
              "label_et": "Keskkonnatervise programm",
              "in": 0,
              "amount": 8095000
            },
            {
              "label_et": "Terviseriskide programm",
              "in": 0,
              "amount": 17758000
            },
            {
              "label_et": "TervishoiusÃ¼steemi programm",
              "in": 0,
              "amount": 1653656000
            }
          ],
          "label_et": "TERVIS",
          "in": 0,
          "amount": 1679509000
        }
      ],
      "label_et": "Sotsiaalmin",
      "customType": "social",
      "in": 125149000,
      "amount": 5867536000
    },
    {
      "label_et": "VABARIIGI VALITSUS",
      "in": 0,
      "amount": 2296155000
    },
    {
      "children": [
        {
          "disabled": [
            {
              "label_et": "DigipÃ¶Ã¶rde programm",
              "in": 0,
              "amount": 14064000
            },
            {
              "label_et": "TÃ¶Ã¶turu ja Ãµppe tihedama seostamise programm",
              "in": 0,
              "amount": 10146000
            },
            {
              "label_et": "KoolivÃµrgu programm",
              "in": 0,
              "amount": 73620000
            },
            {
              "label_et": "Ãœldharidusprogramm",
              "in": 0,
              "amount": 140514000
            },
            {
              "label_et": "Kutseharidusprogramm",
              "in": 0,
              "amount": 98357000
            },
            {
              "label_et": "KÃµrgharidusprogramm",
              "in": 0,
              "amount": 198827000
            },
            {
              "label_et": "TÃ¤iskasvanuhariduse programm",
              "in": 0,
              "amount": 10686000
            },
            {
              "label_et": "Noortevaldkonna programm",
              "in": 0,
              "amount": 11540000
            }
          ],
          "label_et": "HARIDUS",
          "in": 0,
          "amount": 557754000
        },
        {
          "disabled": [
            {
              "label_et": "Keeleprogramm",
              "in": 0,
              "amount": 5592000
            }
          ],
          "label_et": "EESTI KEEL JA EESTLUS",
          "in": 0,
          "amount": 5592000
        },
        {
          "label_et": "TEADUS",
          "in": 0,
          "amount": 175694000
        },
        {
          "disabled": [
            {
              "label_et": "Arhiivinduse programm",
              "in": 0,
              "amount": 8250000
            }
          ],
          "label_et": "RIIGminIVALITSEMINE",
          "in": 0,
          "amount": 8250000
        }
      ],
      "label_et": "Haridus- ja Teadusmin",
      "in": 185092000,
      "amount": 747290000
    },
    {
      "children": [
        {
          "label_et": "ETTEVÃ•TLUS JA INNOVATSIOON",
          "in": 0,
          "amount": 157197000
        },
        {
          "label_et": "TRANSPORT",
          "in": 0,
          "amount": 432895000
        },
        {
          "label_et": "INFOÃœHISKOND",
          "in": 0,
          "amount": 52553000
        },
        {
          "label_et": "ENERGEETIKA",
          "in": 0,
          "amount": 38713000
        }
      ],
      "label_et": "Majandus- ja Kommunikatsioonimin",
      "in": 471033000,
      "amount": 681358000
    },
    {
      "children": [
        {
          "label_et": "JULGEOLEK JA RIIGIKAITSE",
          "in": 0,
          "amount": 517425000
        }
      ],
      "label_et": "Kaitsemin",
      "in": 21912000,
      "amount": 517425000
    },
    {
      "children": [
        {
          "label_et": "PÃ•LLUMAJANDUS JA KALANDUS",
          "in": 0,
          "amount": 418054000
        }
      ],
      "label_et": "Maaelumin",
      "in": 363767000,
      "amount": 418054000
    },
    {
      "children": [
        {
          "label_et": "RIIGIVALITSEMINE",
          "in": 0,
          "amount": 8443000
        },
        {
          "label_et": "SISETURVALISUS JA Ã•IGUSRUUM",
          "in": 0,
          "amount": 374291000
        },
        {
          "disabled": [
            {
              "label_et": "Laste ja perede programm",
              "in": 0,
              "amount": 795000
            }
          ],
          "label_et": "PEREPOLIITIKA",
          "in": 0,
          "amount": 795000
        }
      ],
      "label_et": "Sisemin",
      "in": 52699000,
      "amount": 383530000
    },
    {
      "children": [
        {
          "label_et": "KULTUUR",
          "in": 0,
          "amount": 200102000
        },
        {
          "label_et": "SPORT",
          "in": 0,
          "amount": 44212000
        },
        {
          "label_et": "LÃ•IMUMINE",
          "in": 0,
          "amount": 8990000
        }
      ],
      "label_et": "Kultuurimin",
      "in": 8772000,
      "amount": 253305000
    },
    {
      "children": [
        {
          "label_et": "RIIGIVALITSEMINE",
          "in": 0,
          "amount": 242974000
        }
      ],
      "label_et": "Rahandusmin",
      "in": 10140774000,

    },
    {
      "children": [
        {
          "disabled": [
            {
              "label_et": "Keskkonnakaitse ja -kasutuse programm",
              "in": 0,
              "amount": 167860000
            }
          ],
          "label_et": "KESKKOND",
          "in": 0,
          "amount": 167860000
        },
        {
          "label_et": "PÃ•LLUMAJANDUS JA KALANDUS",
          "in": 0,
          "amount": 5257000
        }
      ],
      "label_et": "Keskkonnamin",
      "in": 408239000,
      "amount": 173117000
    },
    {
      "children": [
        {
          "label_et": "Ã•IGUSKORD",
          "in": 0,
          "amount": 168755000
        }
      ],
      "label_et": "Justiitsmin",
      "in": 41624000,
      "amount": 168755000
    },
    {
      "children": [
        {
          "label_et": "VÃ„LISPOLIITIKA",
          "in": 0,
          "amount": 78088000
        }
      ],
      "label_et": "VÃ¤lisministeerium",
      "in": 7406000,
      "amount": 78088000
    },
    {
      "label_et": "RIIGIKOGU",
      "in": 0,
      "amount": 21555000
    },
    {
      "children": [
        {
          "label_et": "RIIGIVALITSEMINE",
          "in": 0,
          "amount": 10002000
        }
      ],
      "label_et": "Riigikantselei haldusala",
      "in": 1115000,
      "amount": 10002000
    },
    {
      "label_et": "RIIGIKOHUS",
      "in": 220000,
      "amount": 5366000
    },
    {
      "label_et": "RIIGIKONTROLL",
      "in": 0,
      "amount": 5192000
    },
    {
      "label_et": "VABARIIGI PRESIDENT",
      "in": 28000,
      "amount": 4344000
    },
    {
      "label_et": "Ã•IGUSKANTSLER",
      "in": 0,
      "amount": 2730000
    }
  ]
}

window.data.revenue = {
  "id": 1,
  "label": "TULUD 2019",
  "children": [
    {
      "id": 6,
      "label": "Social taxes",
      "customType": "social",
      "label_et": "Sotsiaalkindlustusmaksed",
      "children": [
        {
          "id": 7,
          "label": "Social tax",
          "customType": "social",
          "label_et": "Sotsiaalmaks",
          "amount": 3591200000
        },
        {
          "id": 8,
          "label": "Unemployment insurance tax",
          "customType": "social",
          "label_et": "T\u00f6\u00f6tuskindlustusmakse",
          "amount": 220500000
        },
        {
          "id": 9,
          "label": "Pension fund",
          "customType": "social",
          "label_et": "Kogumispensionimakse (2% t\u00f6\u00f6tasust)",
          "amount": 195100000
        }
      ]
    },
    {
      "id": 2,
      "label": "Value added tax",
      "label_et": "K\u00e4ibemaks",
      "amount": 2648400000
    },
    {
      "id": 20,
      "label": "Income from economic activities",
      "label_et": "Riigi majandustulud",
      "children": [
        {
          "id": 21,
          "label": "Sale of goods and services",
          "label_et": "Kaupade ja teenuste m\u00fc\u00fck",
          "amount": 267100000
        },
        {
          "id": 22,
          "label": "Subsidies",
          "label_et": "Saadud toetused",
          "amount": 1162600000
        },
        {
          "id": 23,
          "label": "Other income",
          "label_et": "Muud tulud",
          "amount": 243000000
        },
        {
          "id": 24,
          "label": "Financial operations (interest and dividends)",
          "label_et": "Finantstulud (intressid ja dividendid)",
          "amount": 152800000
        }
      ]
    },
    {
      "id": 25,
      "label": "Transferrable incomes",
      "label_et": "Edasiantavad tulud",
      "children": [
        {
          "id": 26,
          "label": "Transferrable incomes",
          "label_et": "KOV f\u00fc\u00fcsilise isiku tulumaks",
          "amount": 1302300000
        },
        {
          "id": 27,
          "label": "Transferrable incomes",
          "label_et": "Maamaks",
          "amount": 58000000
        }
      ]
    },
    {
      "id": 10,
      "label": "Excise duties",
      "label_et": "Aktsiisid",
      "children": [
        {
          "id": 11,
          "label": "Alcohol excise duty",
          "label_et": "Alkoholiaktsiis",
          "amount": 228900000
        },
        {
          "id": 12,
          "label": "Tobacco excise duty",
          "label_et": "Tubakaaktsiis",
          "amount": 220000000
        },
        {
          "id": 13,
          "label": "Fuel excise duty",
          "label_et": "K\u00fctuseaktsiis",
          "amount": 568000000
        },
        {
          "id": 14,
          "label": "Electricity excise duty",
          "label_et": "Elektriaktsiis",
          "amount": 36300000
        },
        {
          "id": 15,
          "label": "Electricity excise duty",
          "label_et": "Pakendiaktsiis",
          "amount": 400000
        }
      ]
    },
    {
      "id": 3,
      "label": "Income tax",
      "label_et": "Tulumaks",
      "children": [
        {
          "id": 4,
          "label": "Corporate income tax",
          "label_et": "Juriidilise isiku tulumaks",
          "amount": 439200000
        },
        {
          "id": 5,
          "label": "Personal income tax",
          "label_et": "F\u00fc\u00fcsilise isiku tulumaks",
          "amount": 400900000
        }
      ]
    },
    {
      "id": 16,
      "label": "Other taxes",
      "label_et": "Muud maksud",
      "children": [
        {
          "id": 17,
          "label": "Gambling tax",
          "label_et": "Hasartm\u00e4ngumaks",
          "amount": 28980000
        },
        {
          "id": 18,
          "label": "Customs duties",
          "label_et": "Tollimaks",
          "amount": 50100000
        },
        {
          "id": 19,
          "label": "Heavy vehicle tax",
          "label_et": "Raskeveokimaks",
          "amount": 5300000
        }
      ]
    },
    {
      "label_et": "Defitsiit.",
      "customType": "deficit",
      "amount": 0,
    }
  ]
}
