main();

function main() {
  $('document').ready(function () {
    readTextFile("IncomeData.json", function (rawIncomes) {
      readTextFile("ExpensesData.json", function (rawExpenses) { // siin koha peal on olemas raw failid, millega saab teha step 3)
        readTextFile("InfoForScenarios.json", function (rawScenarios)  {

          var baseBudget = makeBaseBudget(rawIncomes, rawExpenses)
          var scenarios = JSON.parse(rawScenarios)

          var chartObject = makeChart(baseBudget, scenarios);

          initializeAndChangeButtons(baseBudget, scenarios, chartObject)
        })
      })
    });
  });
};

function makeChart(baseBudget, scenarios) {
  var currentBudget = makeCurrentBudget(baseBudget, scenarios); //funktsioon, mis loeb baaseelarvet, stsenaariumid ja lehel valitud maksuprotsendid ja siia tulevad arvutused vastavalt modifikaatoritele (linnukestest)

  displayOverview(currentBudget); // paneb summad jne paika lehele valja kuvamiseks

  // var chartData = parseChartData(currentBudget); //vastavalt modifikaatorile arvutatud currentbudget konverteeritakse charti joonistamiseks sobivasse struktuuri (google neljaliikmeline array)
  var chartData = parseChartData(baseBudget, currentBudget); //vastavalt modifikaatorile arvutatud currentbudget konverteeritakse charti joonistamiseks sobivasse struktuuri (google neljaliikmeline array)

  var chartDataExpanded = expandChart(chartData)

  var chartObject = createChart(chartData, chartDataExpanded);

  return chartObject;//draws the graph
}

function updateChartUpper(baseBudget, scenarios, chartObject) {

  var currentBudget = makeCurrentBudget(baseBudget, scenarios); //funktsioon, mis loeb baaseelarvet, stsenaariumid ja lehel valitud maksuprotsendid ja siia tulevad arvutused vastavalt modifikaatoritele (linnukestest)

  displayOverview(currentBudget); // paneb summad jne paika lehele valja kuvamiseks

  // var chartData = parseChartData(currentBudget); //vastavalt modifikaatorile arvutatud currentbudget konverteeritakse charti joonistamiseks sobivasse struktuuri (google neljaliikmeline array)
  var chartData = parseChartData(baseBudget, currentBudget); //vastavalt modifikaatorile arvutatud currentbudget konverteeritakse charti joonistamiseks sobivasse struktuuri (google neljaliikmeline array)

  var chartDataExpanded = expandChart(chartData)

  updateChart(chartData, chartDataExpanded, chartObject); //draws the graph
}

function parseToBaseBudget(rawIncomes, rawExpenses) {
  var expenses = JSON.parse(rawExpenses);
  var income = JSON.parse(rawIncomes);
  var budget = {
    expenses: expenses,
    income: income
  }
  return budget;
}

function findNodeById(currentBudget, id)  {
  var nodeObject
  var found = false

  var budgetSide;

  if (id.charAt(0) === "E") {
    budgetSide = currentBudget.expenses['Kulu']

  }
  else if (id.charAt(0) === "I")  {
    budgetSide = currentBudget.income['Tulu']
  }


  findNode(budgetSide, id)

  function findNode(budgetSide, id) {

    for(var i = 0; i < budgetSide.length; i++)  {
      if (budgetSide[i].id === id)  {
        nodeObject = budgetSide[i]
        found = true
        break
      } else {
        if (budgetSide[i].components && budgetSide[i].components.length > 0)  {
          findNode(budgetSide[i].components, id)
          if (found)  {
            break
          }
        }
      }
    }
  }

  return nodeObject
}

//function to give value to modifiers based on scenarios selected on webpage and scenario data in json file
function collectmodifiers (basebudget, scenarios) {

  var scienceCurrentPercentage = (findNodeById(basebudget, "Ex2-Teadus").sum) / scenarios.baseBudgetData.GDP;
  var scienceExtraPercentage = 0;
  var extraPensionYearly = 0;
  var parliamentCostConstant = 1;

  if ($('#science1').is(':checked')) {
    scienceExtraPercentage = (scenarios.scenarios.find(function (object) {
      return object.id == "science1"
    }).modifiers.sciencePercentage) - scienceCurrentPercentage;
  }

  if ($('#science2').is(':checked')) {
    scienceExtraPercentage = (scenarios.scenarios.find(function (object) {
      return object.id == "science2"
    }).modifiers.sciencePercentage) - scienceCurrentPercentage;
  }

  if ($('#pension').is(':checked')) {
    extraPensionYearly = scenarios.scenarios.find(function (object) {
      return object.id == "pension"
    }).modifiers.extraPensionYearly;
  };

  if ($('#parliament').is(':checked')) {
    parliamentCostConstant = scenarios.scenarios.find(function (object) {
      return object.id == "parliament"
    }).modifiers.parliamentCostConstant;
  }

  var vatTaxValue = parseInt($('#VAT-tax-output').val());
  var incomeTaxValue = parseInt($('#income-tax-output').val());

  return {
    scienceExtraPercentage: scienceExtraPercentage,
    extraPensionYearly: extraPensionYearly,
    vatTaxValue: vatTaxValue,
    incomeTaxValue: incomeTaxValue,
    parliamentCostConstant: parliamentCostConstant
  }
}

function calculateSum(item)  {
  var itemSum;
  if (item.sum) {
    return item.sum
  }
  else {

    itemSum = item.components.reduce(function (sum, component) {

      return sum + calculateSum(component)

    }, 0)
  }
  return itemSum
}

function sumOfOneSide (side, sideName) {

  var sideSum = 0

  var levelOne = side[sideName].map(function (item) {

    var levelOneSum = calculateSum(item)

    sideSum += levelOneSum

    return sideSum
  })

  return sideSum
}

//make current budget based on scenario data from json file
function makeCurrentBudget(basebudget, scenarios, level) {

  var information = scenarios

  var currentBudget = _.cloneDeep(basebudget);

  var modifiers = collectmodifiers(basebudget, scenarios);

  findNodeById(currentBudget, "Ex2-Teadus").sum += information.baseBudgetData.GDP * modifiers.scienceExtraPercentage;

  //calc  value for social program taking into account extraPension (that was taken from json file in case the checkbox is checked, otherwise 0)

  var pensionRaise = information.baseBudgetData.pensioners * modifiers.extraPensionYearly;

  findNodeById(currentBudget, "Ex3-Sotsiaalkindlustuse_programm").sum += pensionRaise;

  //calculate cost for parliament (depending on checkbox)
  var parliamentCostObject = currentBudget.expenses['Kulu'].find(function (object) {
    return object.id == "Ex1-Riigikogu"
  })

  parliamentCostObject.sum = parliamentCostObject.sum * modifiers.parliamentCostConstant

  //calc new income amount from VAT when value changed with +/-, take into account als change from pensions
  var additionalVATFromPension = pensionRaise * information.scenarios.find(function (scenario) { return scenario.id == "pension"}).modifiers.percentageToVAT
  var additionalIncomeTaxFromPension = pensionRaise * information.scenarios.find(function (scenario) { return scenario.id == "pension"}).modifiers.percentageToIncomeTax

  var currentVATTaxValue = findNodeById(currentBudget, information.structure.vat.level2).sum
  findNodeById(currentBudget, information.structure.vat.level2).sum = ((currentVATTaxValue * modifiers.vatTaxValue) / information.baseBudgetData.VAT) + additionalVATFromPension


  //calc new income amount from income tax when value changed with +/-
  var currentIncomeTaxValueJur = findNodeById(currentBudget, information.structure.incomeTax.level2.level21).sum
  var currentIncomeTaxValueFys = findNodeById(currentBudget, information.structure.incomeTax.level2.level22).sum

  findNodeById(currentBudget, information.structure.incomeTax.level2.level21).sum = (currentIncomeTaxValueJur * modifiers.incomeTaxValue) / information.baseBudgetData.incomeTax
  findNodeById(currentBudget, information.structure.incomeTax.level2.level22).sum = ((currentIncomeTaxValueFys * modifiers.incomeTaxValue) / information.baseBudgetData.incomeTax) + additionalIncomeTaxFromPension

  var expensesSum = sumOfOneSide(currentBudget.expenses, 'Kulu')

  var incomeSum = sumOfOneSide(currentBudget.income, 'Tulu')

  return {
    budget: currentBudget,
    incomeSum: incomeSum,
    expensesSum: expensesSum
  };
}

function createComponents(item, baseItem, parentName) {

  var componentsArray = item.components.map(function (component) {

    var componentSum = calculateSum(component)

    var baseComponent = baseItem.components.find(function (object) {
      return object.name == component.name
    });

    var baseComponentSum = calculateSum(baseComponent)

    //set node color based on change in node size compared to original budget

    var componentName = component.name

    var tooltip = componentName + " " + (componentSum / 1000000).toFixed(1).replace(".", ",") + "mln"

    var itemNodeColor = '#DBD3AD';

    if (componentSum < 20000000)  {
      itemNodeColor = "#37474f"
    }

    if (baseComponentSum < componentSum) {
      itemNodeColor = '#9BC87C'
      tooltip = component.name + "\n" + (baseComponentSum / 1000000).toFixed(1).replace(".", ",") + "mln " + '<span style=\'font-size: large\'>↑</span>' + ((componentSum-baseComponentSum) / 1000000).toFixed(1).replace(".", ",") + "mln"
    }

    else if (baseComponentSum > componentSum) {
      itemNodeColor = '#e74c3c'
      tooltip = component.name + "\n" + (baseComponentSum / 1000000).toFixed(1).replace(".", ",") + "mln " + '<span style=\'font-size: large\'>↓</span>'+ ((baseComponentSum-componentSum) / 1000000).toFixed(1).replace(".", ",") + "mln"
    }

    var componentSubArray = []

    var parentHasTwoLevels = false;

    var hoverStateDeterminer = item.components.map(function (component) {
      if(component.components) {
        parentHasTwoLevels = true
      }
      return " "
    })

    var cursorStyle = am4core.MouseCursorStyle.default
    var hoverState = 0.8

    if(parentHasTwoLevels) {
      cursorStyle = am4core.MouseCursorStyle.pointer
      hoverState = 1
    }

    if (component.components) {
      componentSubArray = createComponents(component, baseComponent, componentName)
    }
    if(item.id.charAt(0) == "E") {

      var componentObject= {
        from: parentName,
        to: componentName,
        id: component.id,
        value: componentSum,
        nodeColor: itemNodeColor,
        tooltip: tooltip,
        sub: componentSubArray,
        clickable: cursorStyle,
        hoverState: hoverState
      }
    }

    else if(item.id.charAt(0) == "I") {
      var componentObject = {
        from: componentName,
        to: parentName,
        id: component.id,
        value: componentSum,
        nodeColor: itemNodeColor,
        tooltip: tooltip,
        sub: componentSubArray,
        hoverState: hoverState

      }
    }

    return componentObject
  })
  return componentsArray
}

function parseChartData(baseBudget, currentBudget)   {

  var expensesArray = currentBudget.budget.expenses['Kulu'].map(function (ex1) {

    var baseEx1 = baseBudget.expenses['Kulu'].find(function (object) {
      return object.name == ex1.name
    });

    //iga ex1 taseme juures arvutab selle ex1 alanejate summa
    var ex1Sum = calculateSum(ex1)

    // //arvutame ka basebudgeti ex1 taseme alanejate summa
    var baseEx1Sum = calculateSum(baseEx1)

    //ex1 leveli nimi, mida kasutame hiljem ex2 objektis "from" väljal
    var ex1Name = ex1.name;

    var tooltip = ex1Name + " " + (ex1Sum / 1000000).toFixed(1).replace(".", ",") + "mln"

    var ex1NodeColor  = '#DBD3AD';

    if (ex1Sum < 20000000)  {
      ex1NodeColor = "#37474f"
    }

    if (baseEx1Sum < ex1Sum) {
      ex1NodeColor = '#9BC87C'
      tooltip = ex1.name + "\n" + (baseEx1Sum / 1000000).toFixed(1).replace(".", ",") + "mln " + '<span style=\'font-size: large\'>↑</span>' + ((ex1Sum-baseEx1Sum) / 1000000).toFixed(1).replace(".", ",") + "mln"
    }

    else if (baseEx1Sum > ex1Sum) {
      ex1NodeColor = '#e74c3c'
      tooltip = ex1.name + "\n" + (baseEx1Sum / 1000000).toFixed(1).replace(".", ",") + "mln " + '<span style=\'font-size: large\'>↓</span>' + ((baseEx1Sum-ex1Sum) / 1000000).toFixed(1).replace(".", ",") + "mln"
    }

    //repeats creating componentarrays for all levels of components
    var ex2Array =[]
    if (ex1.components) {
      ex2Array = createComponents(ex1, baseEx1, ex1Name)
    }

    var ex1Object = {
      from: " ",
      to: ex1Name,
      id: ex1.id,
      value: ex1Sum,
      nodeColor: ex1NodeColor,
      tooltip: tooltip,
      sub: ex2Array,
      clickable: am4core.MouseCursorStyle.pointer,
      hoverState: 1
    }

    return ex1Object
  })

  //parse incomes into parent-subs(-subs) structure for navigatable chart
  var incomeArray = currentBudget.budget.income['Tulu'].map(function (in1) {

    var baseIn1 = baseBudget.income['Tulu'].find(function (item) {
      return item.name == in1.name
    })
    var componentsArray = []

    var baseIn1Sum = calculateSum(baseIn1);

    var in1Sum = calculateSum(in1);

    var in1Name = in1.name

    var tooltip = in1Name + " " + ((in1Sum / 1000000).toFixed(1)).replace(".", ",") + "mln"

    var in1NodeColor = '#DBD3AD'

    if (in1Sum < 20000000)  {
      in1NodeColor = "#37474f"
    }

    if (baseIn1Sum < in1Sum) {
      in1NodeColor = '#9BC87C'
      tooltip = in1.name + "\n" + (baseIn1Sum / 1000000).toFixed(1).replace(".", ",") + "mln " + '<span style=\'font-size: large\'>↑</span>' + ((in1Sum-baseIn1Sum) / 1000000).toFixed(1).replace(".", ",") + "mln"
    }

    else if (baseIn1Sum > in1Sum) {
      in1NodeColor = '#e74c3c'
      tooltip = in1.name + "\n" + (baseIn1Sum / 1000000).toFixed(1).replace(".", ",") + "mln " + '<span style=\'font-size: large\'>↓</span>' + ((baseIn1Sum-in1Sum) / 1000000).toFixed(1).replace(".", ",") + "mln"
    }

    if (in1.components) {

      //puts components into an array to be used as subs
      componentsArray = createComponents(in1, baseIn1, in1Name)
    }

    var parentObject = {
      from: in1Name,
      to: " ",
      id: in1.id,
      value: in1Sum,
      nodeColor: in1NodeColor,
      sub: componentsArray,
      tooltip: tooltip,
      clickable: am4core.MouseCursorStyle.pointer,
      hoverState: 1
    }

    return parentObject
  })

  var centralNodeColor = {
    from: " ",
    nodeColor: "#045556",
    id: "0",
    sub: [],
    tooltip: " ",
    clickable: am4core.MouseCursorStyle.default,
    hoverState: 0.8
  }
  incomeArray.unshift(centralNodeColor)

  var totalBudget = incomeArray.concat(expensesArray)

  //Calculate deficiency/surplus and add needed lines into the parent-subs array. Also add red (deficit) or green (surplus) as nodeColors
  var deficiency = [];
  if (currentBudget.expensesSum > currentBudget.incomeSum) {
    deficiency.push({from: "Defitsiit",
      to: " ", id: "In1-deficit",
      value: (currentBudget.expensesSum - currentBudget.incomeSum),
      nodeColor: '#e74c3c',
      fillOpacity: 1,
      tooltip: "Defitsiit: " + (currentBudget.expensesSum - currentBudget.incomeSum)/1000000 + "mln",
      clickable: am4core.MouseCursorStyle.pointer,
      hoverState: 1,
      sub: [{from: "Defitsiit.",
        to: "Defitsiit",
        id: "In2-deficit",
        value: (currentBudget.expensesSum - currentBudget.incomeSum),
        nodeColor: '#e74c3c', fillOpacity: 1,
        clickable: am4core.MouseCursorStyle.default,
        hoverState: 0.8,
        tooltip: "Defitsiit: " + (currentBudget.expensesSum - currentBudget.incomeSum)/1000000 + "mln"}]})


  } else {
    deficiency.push({from: " ",
      to: "Ülejääk",
      id: "Ex1-surplus",
      value: (currentBudget.incomeSum - currentBudget.expensesSum),
      nodeColor: '#9BC87C',
      fillOpacity: 1,
      tooltip: "Ülejääk: " + (currentBudget.incomeSum - currentBudget.expensesSum)/1000000 + "mln",
      clickable: am4core.MouseCursorStyle.pointer,
      hoverState: 1,
      sub: []})
  }
//{from: "Ülejääk", to: "Ülejääk.",  id: "Ex2-surplus", value: (incomeSum - expensesSum), nodeColor: '#1e8449', fillOpacity: 1}
  var totalBudgetDeficit = totalBudget.concat(deficiency)

  return totalBudgetDeficit;
}

function displayOverview(currentBudget) {

  var incomeSum = currentBudget.incomeSum;
  var expensesSum = currentBudget.expensesSum;

  $("#income-total").val((incomeSum / 1000000000).toFixed(1).replace(".", ","))
  $("#expenses-total").val((expensesSum / 1000000000).toFixed(1).replace(".", ","))

  if ((expensesSum - incomeSum) < 0) {
    $("#deficit").val(((expensesSum - incomeSum) / -1000000).toFixed(1).replace(".", ","))
    $("#percentage").val(((expensesSum - incomeSum) / incomeSum * -100).toFixed(1).replace(".", ","))
    $('#deficit').removeClass("text-danger")
    $('#percentage').removeClass("text-danger")
    $('#deficit-text').text("Ülejääk")
  }
  else if ((expensesSum - incomeSum) > 0) {
    $("#deficit").val(((expensesSum - incomeSum) / 1000000).toFixed(1).replace(".", ","))
    $("#percentage").val(((expensesSum - incomeSum) / incomeSum * 100).toFixed(1).replace(".", ","))
    $('#deficit').addClass("text-danger")
    $('#percentage').addClass("text-danger")
    $('#deficit-text').text("Defitsiit")
  }
}

function makeBaseBudget(rawIncomes, rawExpenses) {
  var baseBudget = parseToBaseBudget(rawIncomes, rawExpenses);//votab json meetodiga rawfiulest yhte objekti kulu ja tulu

  return baseBudget;
}

function initializeAndChangeButtons(baseBudget, scenarios, chartObject) {

  initializeScenarios(scenarios)

  $('#VAT-tax-output').val($('#VAT-tax-slider').val())

  $('#income-tax-output').val($('#income-tax-slider').val())

//change tax values & recalculate chart when tax sliders changed
  $('#VAT-tax-slider').change(function() {
    $('#VAT-tax-output').val($('#VAT-tax-slider').val())
    toggleDefaultButton()
    updateChartUpper(baseBudget, scenarios, chartObject)
  })

  $('#VAT-tax-slider').on('input', function() {
    $('#VAT-tax-output').val($('#VAT-tax-slider').val())
  })

  $('#income-tax-slider').change(function() {
    $('#income-tax-output').val($('#income-tax-slider').val())
    toggleDefaultButton()
    updateChartUpper(baseBudget, scenarios, chartObject)
  })

  $('#income-tax-slider').on('input', function() {
    $('#income-tax-output').val($('#income-tax-slider').val())
  })

  $('.plus-button').on('click', function (event) {
    var siblingRangeTarget = event.currentTarget.previousElementSibling
    var valueRangeSibling = parseInt(siblingRangeTarget.value)
    if (valueRangeSibling >= 10 && valueRangeSibling < 35) {
      siblingRangeTarget.value = valueRangeSibling + 1
      toggleDefaultButton()
      updateChartUpper(baseBudget, scenarios, chartObject)
    }

    var siblingOutputTarget = event.currentTarget.parentElement.nextElementSibling.firstElementChild
      //.previousElementSibling.firstElementChild

    var valueOutputSibling = parseInt(siblingOutputTarget.value)
    if (valueOutputSibling >= 10 && valueOutputSibling < 35) {
      siblingOutputTarget.value = valueOutputSibling + 1
      toggleDefaultButton()
      updateChartUpper(baseBudget, scenarios, chartObject)
    }
  })

  $('.minus-button').on('click', function (event) {
    var siblingRangeTarget = event.currentTarget.nextElementSibling
    var valueRangeSibling = parseInt(siblingRangeTarget.value)
    if (valueRangeSibling > 10 && valueRangeSibling <= 35) {
      siblingRangeTarget.value = valueRangeSibling - 1
      toggleDefaultButton()
      updateChartUpper(baseBudget, scenarios, chartObject)
    }

    var siblingOutputTarget = event.currentTarget.parentElement.nextElementSibling.firstElementChild
    var valueOutputSibling = parseInt(siblingOutputTarget.value)
    if (valueOutputSibling > 10 && valueOutputSibling <= 35) {
      siblingOutputTarget.value = valueOutputSibling - 1
      toggleDefaultButton()
      updateChartUpper(baseBudget, scenarios, chartObject)
    }
  })

  $('#science1').change(function (event) {

    strategyCheckbox(baseBudget, scenarios, "science1", chartObject)
  })

  $('#science2').change(function (event) {

    strategyCheckbox(baseBudget, scenarios, "science2", chartObject)
  })

  $('#pension').change(function (event) {
    strategyCheckbox(baseBudget, scenarios, "pension", chartObject)

  })

  $('#parliament').change(function (event) {

    strategyCheckbox(baseBudget, scenarios, "parliament", chartObject)
  })

  $('#set-default-button').on('click', function (event) {

    defaultButton(scenarios, baseBudget, chartObject)
  })

}

function toggleDefaultButton()   {
  if($('.strategies-listed').length == 0 && $('#income-tax-output').val() == 20 && $('#VAT-tax-output').val() == 20 ) {
    $('#set-default-button').addClass("disabled")
    $('.disclaimer').attr('hidden', true)
  } else {
    $('#set-default-button').removeClass("disabled")
    $('.disclaimer').removeAttr('hidden')
  }
}

function defaultButton(scenarios, baseBudget, chartObject) {

  $('#VAT-tax-slider').val(scenarios.baseBudgetData.VAT)
  $('#income-tax-slider').val(scenarios.baseBudgetData.incomeTax)
  $('#VAT-tax-output').val(scenarios.baseBudgetData.VAT)
  $('#income-tax-output').val(scenarios.baseBudgetData.incomeTax)
  $('#science1').prop('checked', false)
  $('#science2').prop('checked', false)
  $('#pension').prop('checked', false)
  $('#parliament').prop('checked', false)
  $('#set-default-button').addClass("disabled")
  $('.disclaimer').attr('hidden', true)
  $('.scenario-text').removeAttr('hidden')

  const scrollboxNode = document.getElementById("strategies-selected")
  while (scrollboxNode.firstChild) {
    scrollboxNode.removeChild(scrollboxNode.firstChild);
  }

  updateChartUpper(baseBudget, scenarios, chartObject)
}

function strategyCheckbox(baseBudget, scenarios, checkboxName, chartObject) {

  var checkboxId = "#" + checkboxName
  var listId = checkboxName + "-listed"

  const markup = `
    <li class="strategies-listed border-bottom" id=${listId}>${scenarios.scenarios.find(scenario => (scenario.id == checkboxName)).name}</li>
`;

  if ($(checkboxId).prop('checked') == true) {
    $('#set-default-button').removeClass("disabled")
    $('.disclaimer').removeAttr('hidden')
    $("#strategies-selected").append(markup);
    $('.scenario-text').attr('hidden', true)


  }
  else if ($(checkboxId).prop('checked') == false) {
    document.getElementById(listId).remove();

    if($('.strategies-listed').length == 0) {
      $('.scenario-text').removeAttr('hidden')
    }

    if($('.strategies-listed').length == 0 && $('#income-tax-output').val() == 20 && $('#VAT-tax-output').val() == 20 ) {
      $('#set-default-button').addClass("disabled")
      $('.disclaimer').attr('hidden', true)
    }
  }
  updateChartUpper(baseBudget, scenarios, chartObject)

}

function initializeScenarios(scenarios) {

  //populate dropdown with scenarios from json file
  for (i = 0; i < scenarios.scenarios.length; i++) {
    var node = document.createElement("Label")
    var textNode = document.createTextNode(scenarios.scenarios[i].name)
    var nodeInput = document.createElement("Input")
    nodeInput.type = "checkbox"
    nodeInput.id = scenarios.scenarios[i].id
    nodeInput.className = "modifiers"

    node.appendChild(nodeInput)
    node.appendChild(textNode)
    node.className = "dropdown-item"
    document.getElementById("dropdown-menu").appendChild(node)
  }
}

